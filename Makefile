CONTIKI = /home/user/contiki
TARGET=sky
# CFLAGS += -DPROJECT_CONF_H=\"project-conf.h\"
CONTIKI_WITH_RIME = 1
include $(CONTIKI)/Makefile.include
DEFINES=ROUTE_CONF_ENTRIES=120,ROUTE_CONF_DEFAULT_LIFETIME=9999

all: mixed-gw-rt-sync mixed-nn-rt-sync mixed-nn-rt-sync2
mixed-nn: mixed-nn-rt-sync
mixed-gw: mixed-gw-rt-sync
