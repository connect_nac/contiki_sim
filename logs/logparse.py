import datetime
import argparse
from collections import defaultdict

parser = argparse.ArgumentParser(description='Parser of Cooja simulations log for the NaC concept')
parser.add_argument('filename', help='name of the logfile to parse')
parser.add_argument('-i', '--input', type=int, nargs='+', help='mote id of the input nodes in the network')
parser.add_argument('-H', '--hidden', type=int, nargs='+', help='mote id of the hidden neuron nodes in the network')
parser.add_argument('-o', '--output', type=int, nargs='+', help='mote id of the output nodes in the network')
args = parser.parse_args()

inputNodes = args.input
if inputNodes is None:
    inputNodes = args.filename.partition('_i')[2].partition('_')[0].split('-')
    inputNodes = list(map(int, inputNodes))
    print("Detected input nodes:", inputNodes)
numInputs = len(inputNodes)
hiddenNodes = args.hidden
if hiddenNodes is None:
    hiddenNodes = args.filename.partition('_H')[2].partition('_')[0].split('-')
    hiddenNodes = list(map(int, hiddenNodes))
    print("Detected hidden nodes:", hiddenNodes)
numHidden = len(hiddenNodes)
outputNodes = args.output
if outputNodes is None:
    outputNodes = args.filename.partition('_o')[2].partition('_')[0].split('-')
    outputNodes = list(map(int, outputNodes))
    print("Detected output nodes:", outputNodes)
numOutput = len(outputNodes)
nnstats = {}
""" nnstats is a dictionary keeping track of the parsed statistics for the NN
nodes. the keys are the ids of the nodes; for each of those we have a list with
one element per sequence number. Each of this elements is itself a dict with
the relevant statistics for that kind of node.
"""
nnNodes = inputNodes + hiddenNodes + outputNodes
seqNoAfterJump = 0
iotSent = 0
iotReceived = 0
highestSeq = -1
for n in nnNodes:
    nnstats[n] = []


def timestamp_conv(ts):
    try:
        return datetime.datetime.strptime(ts, "%M:%S.%f")
    except ValueError:
        return datetime.datetime.strptime(ts, "%H:%M:%S.%f")


with open(args.filename) as f:
    for line in f:
        if line.startswith('-- Log Listener'):
            continue
        tokens = line.split(maxsplit=2)
        nodeid = int(tokens[1][3:])
        # case 1: sending NN message
        if tokens[2].startswith("Sending NN message"):
            assert(nodeid in inputNodes)
            seqno = int((tokens[2].split())[3])
            # seqno = seqno_hack(seqno, timestamp_conv(tokens[0]))
            if seqno > highestSeq:
                # new NN message, create stats entry for every NN node
                for i in range(seqno - highestSeq):
                    for node in nnNodes:
                        nnstats[node].append(defaultdict(int))
                highestSeq = seqno
            if nnstats[nodeid][seqno]['nnSent'] == 0:
                nnstats[nodeid][seqno]['firstInSent'] = timestamp_conv(tokens[0])
            nnstats[nodeid][seqno]['nnSent'] += 1
            if nnstats[nodeid][seqno]['nnSent'] == numHidden:
                nnstats[nodeid][seqno]['lastInSent'] = timestamp_conv(tokens[0])
        # case 2: Received NN message
        elif tokens[2].startswith("Hidden node received"):
            assert(nodeid in hiddenNodes)
            seqno = int(line.partition('NN')[2].split('\'')[0])
            # seqno = seqno_hack(seqno, timestamp_conv(tokens[0]))
            if nnstats[nodeid][seqno]['nnRcvd'] == 0:
                nnstats[nodeid][seqno]['firstInRcvd'] = timestamp_conv(tokens[0])
            nnstats[nodeid][seqno]['nnRcvd'] += 1
            if nnstats[nodeid][seqno]['nnRcvd'] == numInputs:
                nnstats[nodeid][seqno]['lastInRcvd'] = timestamp_conv(tokens[0])
        # case 3: sending NNH message
        elif tokens[2].startswith("Sending NNH message"):
            assert(nodeid in hiddenNodes)
            seqno = int((tokens[2].split())[3])
            # seqno = seqno_hack(seqno, timestamp_conv(tokens[0]))
            if nnstats[nodeid][seqno]['nnhSent'] == 0:
                nnstats[nodeid][seqno]['firstHidSent'] = timestamp_conv(tokens[0])
            nnstats[nodeid][seqno]['nnhSent'] += 1
            if nnstats[nodeid][seqno]['nnhSent'] == numOutput:
                nnstats[nodeid][seqno]['lastHidSent'] = timestamp_conv(tokens[0])
        # case 4: Received NNH message
        elif tokens[2].startswith("Output node received"):
            assert(nodeid in outputNodes)
            # hack to deal with a logging mismatch that I fixed at some point
            try:
                seqno = int(line.partition('NNH')[2].split('\'')[0])
            except:
                seqno = int(line.split('\'')[1])
            # seqno = seqno_hack(seqno, timestamp_conv(tokens[0]))
            if nnstats[nodeid][seqno]['nnhRcvd'] == 0:
                nnstats[nodeid][seqno]['firstHidRcvd'] = timestamp_conv(tokens[0])
            nnstats[nodeid][seqno]['nnhRcvd'] += 1
            # if nnstats[nodeid][seqno]['nnhRcvd'] == numHidden:
            #    nnstats[nodeid][seqno]['lastHidRcvd'] = timestamp_conv(tokens[0])
        # case 5: all done message
        elif tokens[2].startswith("Received all messages"):
            assert(nodeid in outputNodes)
            seqno = int(tokens[2].split('seq_no ')[1].split(',')[0])
            # seqno = seqno_hack(seqno, timestamp_conv(tokens[0]))
            nnstats[nodeid][seqno]['lastHidRcvd'] = timestamp_conv(tokens[0])
        #TODO: track individual IoT message in case we are using rmh to check for duplicates
        # case 6: generic nodes send IoT message
        elif tokens[2].startswith("Sending RPL"):
            assert(nodeid not in nnNodes)
            iotSent += 1
        # case 7: Gateway received IoT message
        elif tokens[2].startswith("Gateway received"):
            iotReceived += 1

firstout= []
latency = []

# iterate over the data to extract aggregated stats
inputLost = 0
hiddenLost = 0
for i in range(len(nnstats[inputNodes[0]])):
    # first find the timestamp of the last input message sent by any input node for this seqno
    lost = False
    if 'lastInSent' not in nnstats[inputNodes[0]][i]:
        # we never sent enough input packet for this id? mark as lost and carry on
        # print("WARNING: not enough sent input messages for seqno", i, "at input node", inputNodes[0])
        lost = True
    else:
        firstInSent = nnstats[inputNodes[0]][i]['firstInSent']
        lastInSent = nnstats[inputNodes[0]][i]['lastInSent']
        for n in inputNodes[1:]:
            if len(nnstats[n]) > i and 'lastInSent' in nnstats[n][i]:
                firstInSent = min(firstInSent, nnstats[n][i]['firstInSent'])
                lastInSent = max(lastInSent, nnstats[n][i]['lastInSent'])
            else:
                lost = True
    # check if the hidden nodes got all the messages, in order to pinpoint losses
    for n in hiddenNodes:
        if len(nnstats[n]) <= i or 'firstHidSent' not in nnstats[n][i]:
            lost = True
            break
    if lost:
        inputLost += 1
        continue
    # for now forget about the hidden nodes, just calculate end-to-end latency
    firstOutDone = datetime.datetime.max
    lastOutDone = datetime.datetime.min
    lost = False
    for n in outputNodes:
        if len(nnstats[n]) > i and 'lastHidRcvd' in nnstats[n][i]:
            firstOutDone = min(firstOutDone, nnstats[n][i]['lastHidRcvd'])
            lastOutDone = max(lastOutDone, nnstats[n][i]['lastHidRcvd'])
        else:
            lost = True
    if lost is False:
        latency.append(lastOutDone - lastInSent)
        firstout.append(firstOutDone - lastInSent)
    else:
        hiddenLost += 1
print("Total NN messages sent: ", len(nnstats[inputNodes[0]]))
print("*** Average latency:", sum(latency, datetime.timedelta(0))/len(latency))
print("Lost NN messages:", inputLost+hiddenLost, "of which", inputLost, "between input and hidden nodes,", hiddenLost, "between hidden and output nodes")
print("Received", iotReceived, "/", iotSent, "IoT messages (only accurate for mixed or multihop scenarios, no duplicate detection atm)")
