import datetime
import argparse
from collections import defaultdict

parser = argparse.ArgumentParser(description='Parser of Cooja simulations log for the NaC concept')
parser.add_argument('filename', help='name of the logfile to parse')
parser.add_argument('-i', '--input', type=int, nargs='+', help='mote id of the input nodes in the network')
parser.add_argument('-o', '--output', type=int, nargs='+', help='mote id of the output nodes in the network')
parser.add_argument('-g', '--gateway', type=int, help='mote id of the gateway')
args = parser.parse_args()

inputNodes = args.input
if inputNodes is None:
    inputNodes = args.filename.partition('_i')[2].partition('_')[0].split('-')
    inputNodes = list(map(int, inputNodes))
    print("Detected input nodes:", inputNodes)
numInputs = len(inputNodes)
outputNodes = args.output
if outputNodes is None:
    outputNodes = args.filename.partition('_o')[2].partition('_')[0].split('-')
    outputNodes = list(map(int, outputNodes))
    print("Detected output nodes:", outputNodes)
numOutput = len(outputNodes)
gateway = args.gateway
if gateway is None:
    gateway = args.filename.partition('_gw')[2].partition('_g')[2].partition('_')[0] #.split('-')
    gateway = int(gateway)
    print("Detected gateway node:", gateway)
nnstats = {}
""" nnstats is a dictionary keeping track of the parsed statistics for the NN
nodes. the keys are the ids of the nodes; for each of those we have a list with
one element per sequence number. Each of this elements is itself a dict with
the relevant statistics for that kind of node.
"""
nnNodes = inputNodes + [gateway] + outputNodes
for n in nnNodes:
    nnstats[n] = []
highestSeq = -1

def timestamp_conv(ts):
    try:
        return datetime.datetime.strptime(ts, "%M:%S.%f")
    except ValueError:
        return datetime.datetime.strptime(ts, "%H:%M:%S.%f")


with open(args.filename) as f:
    for line in f:
        if line.startswith('-- Log Listener'):
            continue
        tokens = line.split(maxsplit=2)
        nodeid = int(tokens[1][3:])
        # case 1: sending NN message
        if tokens[2].startswith("Sending NN message"):
            assert(nodeid in inputNodes)
            seqno = int((tokens[2].split())[3])            
            if seqno > highestSeq:
                # new NN message, create stats entry for every NN node
                for i in range(seqno - highestSeq):
                    for node in nnNodes:
                        nnstats[node].append(defaultdict(int))
                highestSeq = seqno     
            nnstats[nodeid][seqno]['nnSent'] += 1
            nnstats[nodeid][seqno]['tsSent'] = timestamp_conv(tokens[0])
        # case 2: Received NN message
        elif tokens[2].startswith("Gateway node received 'NN"):
            assert(nodeid == gateway)
            seqno = int(line.partition('NN')[2].split('\'')[0])
            if nnstats[nodeid][seqno]['nnRcvd'] == 0:
                nnstats[nodeid][seqno]['firstInRcvd'] = timestamp_conv(tokens[0])                
            nnstats[nodeid][seqno]['nnRcvd'] += 1
            if nnstats[nodeid][seqno]['nnRcvd'] == numInputs:
                nnstats[nodeid][seqno]['lastInRcvd'] = timestamp_conv(tokens[0])
        # case 3: sending NNH message
        elif tokens[2].startswith("Sending NNH message"):
            assert(nodeid == gateway)
            seqno = int((tokens[2].split())[3])            
            if nnstats[nodeid][seqno]['nnhSent'] == 0:
                nnstats[nodeid][seqno]['firstHidSent'] = timestamp_conv(tokens[0])
            nnstats[nodeid][seqno]['nnhSent'] += 1
            if nnstats[nodeid][seqno]['nnhSent'] == numOutput:
                nnstats[nodeid][seqno]['lastHidSent'] = timestamp_conv(tokens[0])
        # case 4: Received NNH message
        elif tokens[2].startswith("Output node received"):
            assert(nodeid in outputNodes)            
            seqno = int(line.partition('NNH')[2].split('\'')[0])
            # note that it could be a duplicate message, check if the entry exists
            if 'nnhRcvd' not in nnstats[nodeid][seqno]:
                # new NNH message received at output node
                nnstats[nodeid][seqno]['nnhRcvd'] = 1
                nnstats[nodeid][seqno]['tsRcvd'] = timestamp_conv(tokens[0])
            else:
                #duplicate message, ignore
                nnstats[nodeid][seqno]['nnhRcvd'] += 1
                continue

firstout= []
latency = []
# iterate over the data to extract aggregated stats 
inputLost = 0
gwLost = 0
for i in range(len(nnstats[inputNodes[0]])):
    # first find the timestamp of the last input message sent by any input node for this seqno
    lost = False
    if 'tsSent' not in nnstats[inputNodes[0]][i]:  
        # we never sent the enough input packet for this id? mark as lost and carry on
        # print("WARNING: not enough sent input messages for seqno", i, "at input node", inputNodes[0])
        lost = True
    else:
        firstInSent = nnstats[inputNodes[0]][i]['tsSent']
        lastInSent = nnstats[inputNodes[0]][i]['tsSent']        
        for n in inputNodes[1:]:
            if len(nnstats[n]) > i and 'tsSent' in nnstats[n][i]:
                firstInSent = min(firstInSent, nnstats[n][i]['tsSent'])
                lastInSent = max(lastInSent, nnstats[n][i]['tsSent'])
            else:
                lost = True
                break
    if 'lastHidSent' not in nnstats[gateway][i]:
        lost = True
    if lost:
        inputLost += 1
        continue
    # for now forget about the hidden nodes, just calculate end-to-end latency
    firstOutDone = datetime.datetime.max
    lastOutDone = datetime.datetime.min
    lost = False
    for n in outputNodes:
        if 'tsRcvd' in nnstats[n][i]:
            firstOutDone = min(firstOutDone, nnstats[n][i]['tsRcvd'])
            lastOutDone = max(lastOutDone, nnstats[n][i]['tsRcvd'])
        else:
            lost = True
    if lost is False:
        latency.append(lastOutDone - lastInSent)
        firstout.append(firstOutDone - lastInSent)
    else:
        gwLost += 1    
    # if (numOutput > 1):
    #   print("Latency of seqno", i, ":", latency[i], "\t to first complete output:", firstout[i])
    # else:
    #   print("Latency of seqno", i, ":", latency[i])
# trimmed_latency = [l for l in latency if l != datetime.timedelta()]
# print("*** Average latency:", sum(trimmed_latency, datetime.timedelta(0))/len(trimmed_latency))
print("Total NN messages sent: ", len(nnstats[inputNodes[0]]))
print("*** Average latency:", sum(latency, datetime.timedelta(0))/len(latency))
print("Lost NN messages:", inputLost+gwLost, "of which", inputLost, "between input nodes and the gateway,", gwLost, "between the gateway and the output nodes")
