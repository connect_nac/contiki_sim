/*
 * Copyright (c) 2007, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         Latency simulation of the non-NaC approach over a lattice.
 *         This version uses a static routing table read at init time to
 *         figure out what to do a each hop. NN messages are spaced apart
 *         at the input nodes to aovid collisions. They use rmh, while IoT
 *         background messages use mh.
 * \author
 *         Emanuele Di Pascale <dipascae@tcd.ie>
 *         Based on code by Adam Dunkels <adam@sics.se>
 */


#include <stdio.h>
#include <stdlib.h>
#include "contiki.h"
#include "net/rime/rime.h"
#include "lib/random.h"
#include "dev/button-sensor.h"
#include "dev/leds.h"
#include "cfs/cfs.h"
#include "net/rime/route.h"
#include "net/rime/rmh.h"

#define NN_CHANNEL 135
#define IOT_CHANNEL 137
#define GRID_SIZE 7
#define IOT_PERIOD 600
#define NN_PERIOD 60
#define NN_GAP 15
#define INIT_TIME 10
#define MAX_GUARDTIME 2
#define MIN_GUARDTIME 1
#define BUF_SIZE (GRID_SIZE * GRID_SIZE * 9)
#define MAX_RETX 5
#define MAX_HOPS GRID_SIZE*2
#define MAX_INPUT_NODES 5

static const uint8_t input_nodes[] = {29, 44};
static const uint8_t output_nodes[] = {30, 37};
/* Enumeration identifying the type of node in the network */
enum node_type_enum {
  INPUT = 1 << 0,
  HIDDEN = 1 << 1,
  OUTPUT = 1 << 2,
  GATEWAY = 1 << 3,
  GENERIC = 1 << 4
};
static enum node_type_enum node_type;
/*---------------------------------------------------------------------------*/
PROCESS(multihop_grid_process, "setup and initialization");
PROCESS(generic_node_process, "code for a generic IoT node");
PROCESS(input_node_process, "code for an input NN node");
PROCESS(gateway_node_process, "code for the gateway node");
AUTOSTART_PROCESSES(&multihop_grid_process);
/*---------------------------------------------------------------------------*/
/*
 * This function is called at the sink of the collect messages.
 *
static void
recv_coll(const linkaddr_t *originator, uint8_t seqno, uint8_t hops)
{
  printf("Sink got message from %d.%d, seqno %d, hops %d: len %d '%s'\n",
	 originator->u8[0], originator->u8[1],
	 seqno, hops,
	 packetbuf_datalen(),
	 (char *)packetbuf_dataptr());
}
  */
/*---------------------------------------------------------------------------*/
// static const struct collect_callbacks callbacks = { recv_coll };

/* Struct to pass info on the received message to the appropriate process
 */
struct msg_data_t {
  uint16_t seq_no;
  uint8_t sender_id;
};

/* Structure mocking a buffer collecting messages from input nodes; once we
 * have a message from all of them, we can forward a message to the output nodes
 */
struct hidden_buffer {
  uint8_t received; // number of messages received for this seq_no
  uint16_t seq_no;   // current sequence number of the NN message
  uint8_t input_nodes[MAX_INPUT_NODES]; // 1 if we received the corresponding msg
};
static struct hidden_buffer hb;

/*---------------------------------------------------------------------------*/
/*
 * This function is called at the final recepient of the rmh message.
 */
static void
recv(struct rmh_conn *c, linkaddr_t *sender, uint8_t hops)
{
  static char msg[20];
  static char token[4];
  static int rcv_seq_no;
  static int i;
  snprintf(msg, 20, "%s", (char *)packetbuf_dataptr());
  // printf("multihop message received '%s'\n", msg);

  if (node_type & OUTPUT && strncmp(msg, "NNH", 3) == 0) {
    printf("Output node received '%s' message from gateway node %d\n", msg, sender->u8[0]);
    // no need to do anything now, the single message rom the gateway means we are done
  } else if (node_type & GATEWAY) {
    strncpy(token, msg, 3);
    token[3] = '\0';
    if (strncmp(token, "NN", 2) == 0) {
      snprintf(msg, 20, "%s", (char *)packetbuf_dataptr()+2);
      // check that the sequence number is the one we are expecting
      rcv_seq_no = atoi(msg);
      if (rcv_seq_no > hb.seq_no) {
        printf("WARNING: received NN message %d, was expecting %d, assume lost!\n",
               rcv_seq_no, hb.seq_no);
        hb.received = 0;
        hb.seq_no = rcv_seq_no;
        for (i = 0; i < MAX_INPUT_NODES; i++) {
          hb.input_nodes[i] = 0;
        }
      } else if (rcv_seq_no < hb.seq_no) {
        // old packet, we have moved on, discard
        printf("Discarding expired packet %s, current seq_no: %d\n",
               msg, hb.seq_no);
        return;
      }
      // find the index of the input node that sent the message
      for (i = 0; i < sizeof(input_nodes)/sizeof(input_nodes[0])
           && input_nodes[i] != sender->u8[0]; i++);
      if (hb.input_nodes[i] != 0) {
        printf("Ignoring duplicate NN message from sender %d, seqno %d\n",
             sender->u8[0], rcv_seq_no);
      return;
    }
      printf("Gateway node received '%s' message from inpute node %d\n",
              (char*)packetbuf_dataptr(), sender->u8[0]);
      hb.input_nodes[i] = 1;
      hb.received += 1;
      if (hb.received == sizeof(input_nodes)/sizeof(input_nodes[0])) {
        process_post(&gateway_node_process, PROCESS_EVENT_CONTINUE, msg);
      }
    }
  }
}
/*---------------------------------------------------------------------------*/
/*
 * This function is called to forward a packet. it uses the routing table
 * loaded at initialization time from a file to determine the next hop.
 * this ensure that the routing in the simulation is the same as the routing
 * in the optimization framework for the hidden node placement.
 */
static linkaddr_t nexthop;
static struct route_entry * entry;
static linkaddr_t * forward(struct rmh_conn *c,
	const linkaddr_t *originator, const linkaddr_t *dest,
	const linkaddr_t *prevhop, uint8_t hops)
{
  /* Find a neighbor closer to the destination to fwd the message to. */
  entry = route_lookup(dest);
  if (entry == NULL) {
    printf("Could not determine next hop to destinaton %d.%d\n",
           dest->u8[0], dest->u8[1]);
    return NULL;
  }
  nexthop = entry->nexthop;
  if (originator->u8[0] != linkaddr_node_addr.u8[0]) {
    printf("Forwarding message from %d.%d to next hop %d.%d\n",
           originator->u8[0], originator->u8[1],
           nexthop.u8[0], nexthop.u8[1]);
  }
  return &nexthop;
}
static const struct rmh_callbacks nn_call = {recv, forward};
static struct rmh_conn nn_conn;
/*---------------------------------------------------------------------------*/
/*
 * This function is called at the final recepient of the IoT multihop message.
 */
static void
recv_iot(struct multihop_conn *c, const linkaddr_t *sender,
     const linkaddr_t *prevhop, uint8_t hops)
{
  char msg[20];
  snprintf(msg, 20, "%s", (char *)packetbuf_dataptr());
  if (node_type == GATEWAY) {
    printf("Gateway received '%s' message from generic node %d\n", msg, sender->u8[0]);
  }
}
/*---------------------------------------------------------------------------*/
/*
 * This function is called to forward a packet. it uses the routing table
 * loaded at initialization time from a file to determine the next hop.
 * this ensure that the routing in the simulation is the same as the routing
 * in the optimization framework for the hidden node placement.
 */
static linkaddr_t * forward_iot(struct multihop_conn *c,
	const linkaddr_t *originator, const linkaddr_t *dest,
	const linkaddr_t *prevhop, uint8_t hops)
{
  /* Find a neighbor closer to the destination to fwd the message to. */
  entry = route_lookup(dest);
  if (entry == NULL) {
    printf("Could not determine next hop to destinaton %d.%d\n",
           dest->u8[0], dest->u8[1]);
    return NULL;
  }
  nexthop = entry->nexthop;
  if (originator->u8[0] != linkaddr_node_addr.u8[0]) {
    printf("Forwarding message from %d.%d to next hop %d.%d\n",
           originator->u8[0], originator->u8[1],
           nexthop.u8[0], nexthop.u8[1]);
  }
  return &nexthop;
}
static const struct multihop_callbacks iot_call = {recv_iot, forward_iot};
static struct multihop_conn iot_conn;
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(multihop_grid_process, ev, data)
{
  PROCESS_EXITHANDLER(rmh_close(&nn_conn);multihop_close(&iot_conn);)

  PROCESS_BEGIN();

  /* Open a rmh connection on Rime channel NN_CHANNEL to forward NN messages.
   */
  rmh_open(&nn_conn, NN_CHANNEL, &nn_call);

  /* Open a multihop connection on Rime channel IOT_CHANNEL to forward IoT messages.
   */
  multihop_open(&iot_conn, IOT_CHANNEL, &iot_call);

  /* Determine what kind of node we are */
  static int i;
  node_type = 0;
  for (i = 0; i < sizeof(input_nodes)/sizeof(input_nodes[0]); i++) {
    if (linkaddr_node_addr.u8[0] == input_nodes[i]) {
      node_type = node_type | INPUT;
      leds_toggle(LEDS_BLUE);
      printf("I am an input node\n");
    }
  }
  for (i = 0; i < sizeof(output_nodes)/sizeof(output_nodes[0]); i++) {
    if (linkaddr_node_addr.u8[0] == output_nodes[i]) {
      node_type = node_type | OUTPUT;
      leds_toggle(LEDS_RED);
      printf("I am an output node\n");
    }
  }
  if (linkaddr_node_addr.u8[0] == (GRID_SIZE * GRID_SIZE +1)/2) {
    /* The gateway is the node in the middle of the grid */
    node_type =  node_type | GATEWAY;
    printf("I am the gateway/sink\n");
    // collect_set_sink(&tc, 1);
  }  
  if (node_type == 0) {
    node_type = GENERIC;
    printf("I am a generic node\n");
  }

  /* Read routing table from file */
  route_init();
  static int fd;
  static char filename[4];
  snprintf(filename, 4, "%d", linkaddr_node_addr.u8[0]);
  fd = cfs_open(filename, CFS_READ);
  if (fd >= 0) {
    /* this must be enough to read the whole file, as apparently cfs does not
     * offer a scan method, and we are forced to read a fixed amount of bytes.
     * Another option could be reading one line at a time, but the lines must
     * have a fixed byte length (i.e. 2 or 3 digits with leading 0s if needed)
     */

    static char buf[BUF_SIZE];
    cfs_read(fd, buf, sizeof(buf));
    cfs_close(fd);
    static char tkn[4];
    i = 0;
    static int j = 0;
    static int sq = 0;
    static int k = 0;
    static linkaddr_t dest;
    static linkaddr_t nhop;
    while(i<BUF_SIZE && k < (GRID_SIZE*GRID_SIZE)-1) {
      // scan for destination
      for (j = 0; buf[i] >= '0' && buf[i] <= '9' && i<BUF_SIZE; j++) {
        tkn[j] = buf[i];
        i++;
      }
      if (i<BUF_SIZE) {
        tkn[j] = '\0';
        dest.u8[0] = atoi(tkn);
        dest.u8[1] = 0;
        i++; // burn the whitespace
      } else {
        printf("Prematurely reached end of buffer!\n");
        break;
      }
      // scan for nexthop
      for (j = 0; buf[i] >= '0' && buf[i] <= '9' && i<BUF_SIZE; j++) {
        tkn[j] = buf[i];
        i++;
      }
      if (i<BUF_SIZE) {
        tkn[j] = '\0';
        nhop.u8[0] = atoi(tkn);
        nhop.u8[1] = 0;
        route_add(&dest, &nhop, 1, sq); // 1 is the cost, assumed unitary
        sq++; // increase sequence number, whatever they are used for
        i++; // burn the whitespace
        i++; // burn the \n
      } else {
        printf("Prematurely reached end of buffer!\n");
        break;
      }
    k++; // next row
    }
  } else {
    printf("**** Could not read from file %s ****\n", filename);
  }
  route_set_lifetime(9999);

  /* Behavior depends on the node type */
  switch(node_type) {
    case GENERIC:
      process_start(&generic_node_process, NULL);
      break;
    case INPUT:
      process_start(&input_node_process, NULL);
      break;
    case GATEWAY:
      process_start(&gateway_node_process, NULL);
      break;
    default:
      break; // do nothing
  }
  /* Wait */
  while(1) {
    PROCESS_WAIT_EVENT();
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
/* Code for a generic node:
 * Send a collect packet on average every IOT_PERIOD seconds.
 */
PROCESS_THREAD(generic_node_process, ev, data)
{
  static struct etimer periodic_gen;
  static struct etimer et_gen;
  static linkaddr_t gw;
  static uint16_t seq_no = 0;

  PROCESS_BEGIN();
  gw.u8[0] = (GRID_SIZE * GRID_SIZE +1)/2;
  gw.u8[1] = 0;
  /* Allow some time for the network to settle. */
  etimer_set(&et_gen, INIT_TIME * CLOCK_SECOND);
  PROCESS_WAIT_UNTIL(etimer_expired(&et_gen));
  while(IOT_PERIOD > 0) {
    if(etimer_expired(&periodic_gen)) {
      etimer_set(&periodic_gen, CLOCK_SECOND * IOT_PERIOD);
      etimer_set(&et_gen, random_rand() % (CLOCK_SECOND * IOT_PERIOD));
    }
    PROCESS_WAIT_EVENT();
    if(etimer_expired(&et_gen)) {
      printf("Sending RPL packet to Gateway %d\n", gw.u8[0]);
      packetbuf_clear();
      packetbuf_set_datalen(sprintf(packetbuf_dataptr(),
	                            "IoT%d", seq_no) + 1);
      multihop_send(&iot_conn, &gw);
      seq_no++;
      PROCESS_WAIT_UNTIL(etimer_expired(&periodic_gen));
    }
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
/* Code for an input node:
 * Send a multihop packet on average every NN_PERIOD seconds.
 */
PROCESS_THREAD(input_node_process, ev, data)
{
  static struct etimer periodic_in;
  static struct etimer et_in;
  static linkaddr_t gw;
  static uint8_t sync_order;
  static int i;
  PROCESS_BEGIN();
  gw.u8[0] = (GRID_SIZE * GRID_SIZE +1)/2;
  gw.u8[1] = 0;
  /* Allow some time for the network to settle. */
  etimer_set(&et_in, INIT_TIME * CLOCK_SECOND);
  PROCESS_WAIT_UNTIL(etimer_expired(&et_in));
  // Find the index of this node in the input nodes array
  for (i = 0; i < sizeof(input_nodes)/sizeof(input_nodes[0])
         && input_nodes[i] != linkaddr_node_addr.u8[0]; i++);
  sync_order = i;
  static uint16_t seq_no = 0;
  while(1) {
    if(etimer_expired(&periodic_in)) {
      etimer_set(&periodic_in, CLOCK_SECOND * NN_PERIOD);
      // first node waits 0*NN_GAP, second waits 1*NN_GAP etc.
      etimer_set(&et_in, sync_order * NN_GAP * CLOCK_SECOND);
    }
    /* Wait until a timer expires */
    PROCESS_WAIT_EVENT();
    if(etimer_expired(&et_in)) {
      /* Send message to gateway node */
      packetbuf_clear();
      packetbuf_set_datalen(sprintf(packetbuf_dataptr(),
	                              "NN%d", seq_no) + 1);
      printf("Sending NN message %d to gateway node %d.%d\n",
           seq_no, gw.u8[0], gw.u8[1]);
      rmh_send(&nn_conn, &gw, MAX_RETX, MAX_HOPS);
    }
    seq_no += 1;
    if (!etimer_expired(&periodic_in)) {
      PROCESS_WAIT_UNTIL(etimer_expired(&periodic_in));
    }
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/

/* Code for the gateway node:
 * Collect input messages, send a message to output nodes when we have enough.
 */
PROCESS_THREAD(gateway_node_process, ev, data)
{
  static int i;
  static linkaddr_t to;
  static struct etimer et_gw;
  static clock_time_t wait;
  PROCESS_BEGIN();
  hb.received = 0;
  hb.seq_no = 0;
  for (i = 0; i < MAX_INPUT_NODES; i++) {
    hb.input_nodes[i] = 0;
  }
  /* Wait until we get an event signifying that we received a message from
   * an input node
   */
  while(1) {
    PROCESS_YIELD();
    // Send a message to all output nodes
    for (i = 0; i < sizeof(output_nodes)/sizeof(output_nodes[0]); i++) {
      // wait a random amount of time between MIN_GUARDTIME and MAX_GUARDTIME
      // but no need to wait before the first message!
      if (i !=0) {
        wait = 0;
        while (wait < CLOCK_SECOND * MIN_GUARDTIME) {
          wait = random_rand() % (CLOCK_SECOND * MAX_GUARDTIME);
        }
        etimer_set(&et_gw, wait);
        PROCESS_WAIT_UNTIL(etimer_expired(&et_gw));
      }
      to.u8[0] = output_nodes[i];
      to.u8[1] = 0;
      packetbuf_clear();
      packetbuf_set_datalen(sprintf(packetbuf_dataptr(),
	                           "NNH%d", hb.seq_no) + 1);
      printf("Sending NNH message %d to output node %d.%d\n",
             hb.seq_no, to.u8[0], to.u8[1]);
      rmh_send(&nn_conn, &to, MAX_RETX, MAX_HOPS);
    }
    hb.received = 0;
    hb.seq_no += 1;
    for (i = 0; i < MAX_INPUT_NODES; i++) {
      hb.input_nodes[i] = 0;
    }
  }
  PROCESS_END();
}

/*---------------------------------------------------------------------------*/
