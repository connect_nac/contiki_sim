/*
 * Copyright (c) 2007, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         Latency simulation of the NaC concept over a lattice.
 * \author
 *         Emanuele Di Pascale <dipascae@tcd.ie>
 *         Based on code by Adam Dunkels <adam@sics.se>
 */

#include "contiki.h"
#include "net/rime/rime.h"
#include "net/rime/collect.h"
#include "net/rime/rmh.h"
#include "lib/random.h"
#include "dev/button-sensor.h"
#include "dev/leds.h"
#include "cfs/cfs.h"
#include "shell.h"
#include "serial-shell.h"
#include "collect-view.h"
#include <stdio.h>

#define CHANNEL 135
#define GRID_SIZE 5
#define IOT_PERIOD 120
#define NN_PERIOD 45
#define INIT_TIME 120
#define MAX_RETX 5
#define MAX_HOPS GRID_SIZE*2

static struct collect_conn tc;
static const uint8_t input_nodes[] = {5, 9};
static const uint8_t hidden_nodes[] = {4, 10};
static const uint8_t output_nodes[] = {3};
/* Enumeration identifying the type of node in the network */
enum node_type_enum {
  INPUT,
  HIDDEN,
  OUTPUT,
  GATEWAY,
  GENERIC
};
static enum node_type_enum node_type; 
/*---------------------------------------------------------------------------*/
PROCESS(multihop_grid_process, "setup and initialization");
PROCESS(generic_node_process, "code for a generic IoT node");
PROCESS(input_node_process, "code for an input NN node");
PROCESS(hidden_node_process, "code for an hidden NN node");
PROCESS(collect_view_shell_process, "Contiki Collect View Shell");
//PROCESS(output_node_process, "code for an output NN node");
//PROCESS(gateway_process, "code for the gateway node");
AUTOSTART_PROCESSES(&multihop_grid_process, &collect_view_shell_process);
/*---------------------------------------------------------------------------*/
/*
 * This function is called at the sink of the collect messages.
 */
static void
recv_coll(const linkaddr_t *originator, uint8_t seqno, uint8_t hops)
{
  printf("Sink got message from %d.%d, seqno %d, hops %d: len %d '%s'\n",
	 originator->u8[0], originator->u8[1],
	 seqno, hops,
	 packetbuf_datalen(),
	 (char *)packetbuf_dataptr());
}
/*---------------------------------------------------------------------------*/
static const struct collect_callbacks callbacks = { recv_coll };

/*
 * This function is called at the final recepient of the rmh message.
 */
static void
recv_mhop(struct rmh_conn *c, const linkaddr_t *sender,
          uint8_t hops)
{
  char msg[20];
  snprintf(msg, 20, "%s", (char *)packetbuf_dataptr());
  printf("multihop message received '%s'\n", msg);
  
  if (node_type == HIDDEN) {
    /* wake up the hidden node to process the input message */
    process_post(&hidden_node_process, PROCESS_EVENT_CONTINUE, msg);    
  }
}
/*---------------------------------------------------------------------------*/
/*
 * This function is called to forward a packet. The function picks a
 * random neighbor from the neighbor list and returns its address. The
 * multihop layer sends the packet to this address. If no neighbor is
 * found, the function returns NULL to signal to the multihop layer
 * that the packet should be dropped.
 */
static linkaddr_t nexthop;
static linkaddr_t * forward(struct rmh_conn *c,
	const linkaddr_t *originator, const linkaddr_t *dest,
	const linkaddr_t *prevhop, uint8_t hops)
{
  /* Find a neighbor closer to the destination to fwd the message to. */
  nexthop.u8[1] = 0;
  /* Try to move on the grid towards the destination, first horizontally,
   * and then vertically. 
   */
  if ((dest->u8[0] - 1) / GRID_SIZE > (linkaddr_node_addr.u8[0] - 1) / GRID_SIZE) {
    /* forward to neighbor to the right on the grid */
    nexthop.u8[0] = linkaddr_node_addr.u8[0] + GRID_SIZE;
  } else if ((dest->u8[0] - 1) / GRID_SIZE < (linkaddr_node_addr.u8[0] - 1) / GRID_SIZE) {
    /* forward to neighbor to the left on the grid */
    nexthop.u8[0] = linkaddr_node_addr.u8[0] - GRID_SIZE;
  } else if ((dest->u8[0] - 1) % GRID_SIZE > (linkaddr_node_addr.u8[0] - 1) % GRID_SIZE) {
    /* forward to neighbor down on the grid */
    nexthop.u8[0] = linkaddr_node_addr.u8[0] + 1;
  } else if ((dest->u8[0] - 1) % GRID_SIZE < (linkaddr_node_addr.u8[0] - 1) % GRID_SIZE) {
    /* forward to neighbor up on the grid */
    nexthop.u8[0] = linkaddr_node_addr.u8[0] - 1;
  } else {
    /* We should never end up here */
    printf("%d.%d: Could not determine next hop to %d.%d\n",
           linkaddr_node_addr.u8[0], linkaddr_node_addr.u8[1],
           dest->u8[0], dest->u8[1]);
    return NULL;
  }
  if (originator->u8[0] != linkaddr_node_addr.u8[0]) {
    printf("%d.%d: forwarding message to next hop %d.%d\n",
           linkaddr_node_addr.u8[0], linkaddr_node_addr.u8[1],
           nexthop.u8[0], nexthop.u8[1]);
  }
  return &nexthop;
}
static const struct rmh_callbacks rmh_call = {recv_mhop, forward};
static struct rmh_conn rmh;
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(multihop_grid_process, ev, data)
{  
  PROCESS_EXITHANDLER(rmh_close(&rmh);)
    
  PROCESS_BEGIN();

  /* Open a rmh connection on Rime channel CHANNEL to forward NN messages.
   */
  rmh_open(&rmh, CHANNEL, &rmh_call); 
  
  /* Open a collect connection for the messages to the gateway */
  collect_open(&tc, 130, COLLECT_ROUTER, &callbacks);
  
  /* Determine what kind of node we are */
  int i;
  node_type = GENERIC;
  for (i = 0; node_type == GENERIC && 
              i < sizeof(input_nodes)/sizeof(input_nodes[0]); i++) {
    if (linkaddr_node_addr.u8[0] == input_nodes[i]) {
      node_type = INPUT;
      leds_toggle(LEDS_BLUE);
    }
  }
  for (i = 0; node_type == GENERIC && 
              i < sizeof(hidden_nodes)/sizeof(hidden_nodes[0]); i++) {
    if (linkaddr_node_addr.u8[0] == hidden_nodes[i]) {
      node_type = HIDDEN;
      leds_toggle(LEDS_GREEN);
    }
  }
  for (i = 0; node_type == GENERIC && 
              i < sizeof(output_nodes)/sizeof(output_nodes[0]); i++) {
    if (linkaddr_node_addr.u8[0] == output_nodes[i]) {
      node_type = OUTPUT;
      leds_toggle(LEDS_RED);
    }
  }
  if (node_type == GENERIC && 
      linkaddr_node_addr.u8[0] == (GRID_SIZE * GRID_SIZE +1)/2) {
    /* The gateway is the node in the middle of the grid */
    node_type = GATEWAY;
    printf("I am the gateway/sink\n");
    /* test the reading from file */
    int fd;
    fd = cfs_open("test.txt", CFS_READ);
    if (fd >= 0) {
      char buf[1024];      
      cfs_read(fd, buf, sizeof(buf));
      printf("**** Message from file: %s ****\n", buf);
      cfs_close(fd);
    } else {
      printf("**** Could not read from file! ****\n");
    }
    collect_set_sink(&tc, 1);
  }
  
  /* Behavior depends on the node type */
  switch(node_type) {
    case GENERIC:      
      process_start(&generic_node_process, NULL);
      break;
    case INPUT:
      process_start(&input_node_process, NULL);        
      break;
    case HIDDEN:
      process_start(&hidden_node_process, NULL);
      break;
    default:
      break; // do nothing
  }
  /* Wait */
  while(1) {
    PROCESS_WAIT_EVENT();
  }           
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
/* Code for a generic node:
 * Send a collect packet on average every IOT_PERIOD seconds. 
 */ 
PROCESS_THREAD(generic_node_process, ev, data)
{
  static struct etimer periodic;
  static struct etimer et;
  PROCESS_BEGIN();  
  /* Allow some time for the network to settle. */
  etimer_set(&et, INIT_TIME * CLOCK_SECOND);
  PROCESS_WAIT_UNTIL(etimer_expired(&et));
  while(1) {        
    if(etimer_expired(&periodic)) {
      etimer_set(&periodic, CLOCK_SECOND * IOT_PERIOD);
      etimer_set(&et, random_rand() % (CLOCK_SECOND * IOT_PERIOD));
    }
    PROCESS_WAIT_EVENT();
    if(etimer_expired(&et)) {
      static linkaddr_t oldparent;
      const linkaddr_t *parent;
      printf("Sendingl\n");
      packetbuf_clear();
      packetbuf_set_datalen(sprintf(packetbuf_dataptr(),
	                            "%s", "Hello") + 1);
      collect_send(&tc, 15);
      parent = collect_parent(&tc);
      if(!linkaddr_cmp(parent, &oldparent)) {
        if(!linkaddr_cmp(&oldparent, &linkaddr_null)) {
          printf("#L %d 0\n", oldparent.u8[0]);
        }
        if(!linkaddr_cmp(parent, &linkaddr_null)) {
          printf("#L %d 1\n", parent->u8[0]);
        }
      linkaddr_copy(&oldparent, parent);
      }
    }
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
/* Code for an input node:
 * Send a multihop packet on average every NN_PERIOD seconds. 
 */ 
PROCESS_THREAD(input_node_process, ev, data)
{
  static struct etimer periodic;
  static struct etimer et;
  PROCESS_BEGIN();  
  /* Allow some time for the network to settle. */
  etimer_set(&et, INIT_TIME * CLOCK_SECOND);
  PROCESS_WAIT_UNTIL(etimer_expired(&et));
  int i;
  static uint8_t seq_no = 0;
  while(1) {
    linkaddr_t to;    
    if(etimer_expired(&periodic)) {
      etimer_set(&periodic, CLOCK_SECOND * NN_PERIOD);
      etimer_set(&et, random_rand() % (CLOCK_SECOND * NN_PERIOD));
    }
    /* Wait until a timer expires */
    PROCESS_WAIT_EVENT();
    if(etimer_expired(&et)) {
      /* Send message to all hidden nodes */
      char buf[20];
      snprintf(buf, 20, "NN%d", seq_no);
      for (i = 0; i < sizeof(hidden_nodes)/sizeof(hidden_nodes[0]); i++) {
        packetbuf_copyfrom(buf, 20);
        to.u8[0] = hidden_nodes[i];
        to.u8[1] = 0;
        printf("%d.%d: Sending NN message %d to hidden node %d.%d\n",
           linkaddr_node_addr.u8[0], linkaddr_node_addr.u8[1],
           seq_no, to.u8[0], to.u8[1]);
        rmh_send(&rmh, &to, MAX_RETX, MAX_HOPS);
      }
      seq_no += 1;
    }
  }        
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
/* Structure mocking a buffer collecting messages from input nodes; once we
 * have a message from all of them, we can forward a message to the output nodes 
 */
struct hidden_buffer {
  uint8_t received;
  uint8_t seq_no;
};
/* Code for a hidden node:
 * Collect input messages, send a message to output nodes when we have enough. 
 */ 
PROCESS_THREAD(hidden_node_process, ev, data)
{
  static struct hidden_buffer hb;
  PROCESS_BEGIN();
  hb.received = 0;
  hb.seq_no = 0;
  int i;
  linkaddr_t to;
  /* Wait until we get an event signifying that we received a message from 
   * an input node
   */
  while(1) {
    PROCESS_YIELD();
    // check that the sequence number is the one we are expecting
    int rcv_seq_no = 0;        
    rcv_seq_no = atoi(((char*)data)+2);
    if (rcv_seq_no > hb.seq_no) {
      printf("WARNING: received NN message %d, was expecting %d, assume lost!\n",
             rcv_seq_no, hb.seq_no);
      hb.received = 0;
      hb.seq_no = rcv_seq_no;
    }
    hb.received += 1;
    if (hb.received == sizeof(input_nodes)/sizeof(input_nodes[0])) {
      hb.received = 0;
      char buf[20];
      snprintf(buf, 20, "NN_H%d", hb.seq_no);
      // Send a message to all output nodes     
      for (i = 0; i < sizeof(output_nodes)/sizeof(output_nodes[0]); i++) {
        to.u8[0] = output_nodes[i];
        to.u8[1] = 0;
        packetbuf_copyfrom(buf, 20);
        printf("%d.%d: Sending NN_H message %d to output node %d.%d\n",
           linkaddr_node_addr.u8[0], linkaddr_node_addr.u8[1],
           hb.seq_no, to.u8[0], to.u8[1]);     
        rmh_send(&rmh, &to, MAX_RETX, MAX_HOPS);
      }
      hb.seq_no += 1;
    }
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(collect_view_shell_process, ev, data)
{
  PROCESS_BEGIN();

  serial_shell_init();
  shell_blink_init();

/*
#if WITH_COFFEE
  shell_file_init();
  shell_coffee_init();
#endif /* WITH_COFFEE */

  /* shell_download_init(); */
  /* shell_rime_sendcmd_init(); */
  /* shell_ps_init(); */
  shell_reboot_init();
  shell_rime_init();
  shell_rime_netcmd_init();
  /* shell_rime_ping_init(); */
  /* shell_rime_debug_init(); */
  /* shell_rime_debug_runicast_init(); */
  shell_powertrace_init();
  /* shell_base64_init(); */
  shell_text_init();
  shell_time_init();
  /* shell_sendtest_init(); */

#if CONTIKI_TARGET_SKY
  shell_sky_init();
#endif /* CONTIKI_TARGET_SKY */

  shell_collect_view_init();

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
